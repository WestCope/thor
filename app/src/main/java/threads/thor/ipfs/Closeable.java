package threads.thor.ipfs;


public interface Closeable {
    boolean isClosed();
}
