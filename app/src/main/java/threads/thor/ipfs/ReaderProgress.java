package threads.thor.ipfs;

public interface ReaderProgress extends Progress {
    long getSize();
}
